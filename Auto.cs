﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingCSharp
{
    public class Auto
    {
        public Auto(string nr = "", Silnik.Typ t = Silnik.Typ.Nieznany,
                     double m = 0, Nadwozie.Kolor k = Nadwozie.Kolor.Nieznany)
        {
            nrRej = nr;
            silnik = new Silnik(t, m);
            nadwozie = new Nadwozie(k);
        }


        public string nrRej;
        public Silnik silnik;
        public Nadwozie nadwozie;
    }
}
