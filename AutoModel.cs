﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingCSharp
{
    public class AutoModel
    {
        public AutoModel(string rejestracja, string dataWjazdu, string nadwozie)
        {
            Rejestracja = rejestracja;
            DataWjazdu = dataWjazdu;
            Nadwozie = nadwozie;
        }

        public string Rejestracja { get; set; }
        public string DataWjazdu { get; set; }
        public string Nadwozie { get; set; }

        public override string ToString()
        {
            return string.Format("Nr.rejestracyjny {0}, Data wjazdu {1}, Kolor karoserii {2}",
                Rejestracja, DataWjazdu, Nadwozie);
        }

    }
}
