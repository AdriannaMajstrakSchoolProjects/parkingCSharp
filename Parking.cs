﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingCSharp
{
    public class Parking
    {
        public Parking()
        {
            auta = new List<Auto>();
            slownikNrRejCzasStart = new Dictionary<string, DateTime>();
        }

        public bool dodaj(Auto a)
        {
            try
            {
                if(czyAutoIstnieje(a.nrRej))
                {
                    return false;
                }      

                auta.Add(a);
                slownikNrRejCzasStart.Add(a.nrRej, DateTime.Now);
                return true;
            }
            catch (System.ArgumentException)
            {
                return false;
                
            }
            
        }

        public int lbAut()
        {
            return auta.Count();
        }

        public void kasujAuta()
        {
            auta.Clear();
            slownikNrRejCzasStart.Clear();
        }

        public void usunAuto(Auto doUsuniecia)
        {
            auta.Remove(doUsuniecia);
            slownikNrRejCzasStart.Remove(doUsuniecia.nrRej);
        }

        public Auto znajdzAuto(string nr)
        {
            for (int i = 0; i < this.lbAut(); i++)
            {
                if (auta[i].nrRej==nr)
                {
                    return auta[i];
                }

            }

            throw new System.Data.ObjectNotFoundException();
        }

        public bool czyAutoIstnieje(string rejestracja)
        {
            for (int i = 0; i < this.lbAut(); i++)
            {
                if (auta[i].nrRej == rejestracja)
                {
                    return true;
                }

            }

            return false;
        }
        
        public List<Auto> GetAuta()
        {
            return auta;
        }

        //public List<Auto> Auta
        //{
        //    get
        //    {
        //        return auta;
        //    }
        //}
      
         public TimeSpan podajIleCzasuSpedzil(string rejestracja)
        {
            var czasSpedzony = DateTime.Now - slownikNrRejCzasStart[rejestracja];
            return czasSpedzony;
        }

        public double podajOplate(double czasSpedzony)
        {
            double oplataZaMinute=0.10;
            return Math.Round(czasSpedzony * oplataZaMinute, 2);
        }

        public Dictionary<string,DateTime> GetSlownikNrRejCzas()
        {
            return slownikNrRejCzasStart;
        }
        Dictionary<string, DateTime> slownikNrRejCzasStart;

        List<Auto> auta;        
    }
}
