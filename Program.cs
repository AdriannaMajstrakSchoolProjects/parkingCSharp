﻿// Programowanie obiektowe
// Definiowanie prostych klas, wykorzystanie zwiazkow
// pomiedzy klasami w postaci agregacji i kompozycji.
//
// Proste demo -- agregacja i kompozycja klas na przykladzie
// obiektow Auto, Silnik, Nadwozie, Parking.
//
// Cwiczenie 1
// Dokonczyc program:
//  -- brakuje funkcji rejestrowania wjazdu/wyjazdu z parkingu,
//  -- brakuje funkcji przegladu zawartosci parkingu,
//  -- dodac rejestracje godziny wjazdu,
//  -- przy wyjezdzie wyznaczac czas postoju na parkingu,
//  -- dodac naliczanie oplaty parkingowej pobieranej przy wyjezdzie
//     na podstawie wyznaczonego czasu parkowania,
//
// Cwiczenie 2
// Rozbudowac program o przechowywanie zawartosci parkingu w pliku,
// tak aby po ponownym uruchomieniu programu poprzednia zawartosc
// byla odczytywana.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParkingCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            NadzorParkingu np = new NadzorParkingu();
            np.dzialaj();
        }

    }
}

