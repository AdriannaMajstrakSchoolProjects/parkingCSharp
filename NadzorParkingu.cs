﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingCSharp
{
    public class NadzorParkingu
    {
        public NadzorParkingu()
        {
            parking = new Parking();
            Plik.OdczytZPliku(parking.GetAuta(), parking.GetSlownikNrRejCzas());
        }

        public void dzialaj()
        {
            ConsoleKeyInfo klawisz;
            do
            {


                menu();
                klawisz = Console.ReadKey();
                switch (klawisz.Key)
                {
                    case ConsoleKey.D1:
                        rejestrujWjazd();
                        break;
                    case ConsoleKey.D2:
                        rejestrujWyjazd();
                        break;
                    case ConsoleKey.D3:
                        przegladParkingu();
                        break;
                }
            }
            while (klawisz.Key != ConsoleKey.D4);

            ZapiszStanParkingu();
        }

        public void ZapiszStanParkingu()
        {
            Plik.ZapiszDoPliku(parking.GetAuta(), parking.GetSlownikNrRejCzas());
        }

        public void menu()
        {
            Console.WriteLine("");
            Console.WriteLine("1. Rejestruj wjazd");
            Console.WriteLine("2. Rejestruj wyjazd");
            Console.WriteLine("3. Pojazdy na parkingu");
            Console.WriteLine("4. Koniec");
            Console.Write("?>");
        }

        public void rejestrujWjazd()
        { 
            Console.WriteLine("\nWjazd");
            Console.WriteLine("\nPodaj nr rejestracyjny");

            var rejestracja = Console.ReadLine();


            if (string.IsNullOrWhiteSpace(rejestracja))
            {
                Console.WriteLine("Numer rejestracyjny nie może być pusty ani się powtarzać");
            }
            else
            {
                parking.dodaj(new Auto(rejestracja));
            }
            czekajNaEnter();            
        }

        void rejestrujWyjazd()
        {
            Console.WriteLine("\nWyjazd");
            Console.WriteLine("\nPodaj nr rejestracyjny");


            try
            {
                string rejestracja = Console.ReadLine();
                double minuty = parking.podajIleCzasuSpedzil(rejestracja).TotalMinutes;
                Auto wyjezdzajace = parking.znajdzAuto(rejestracja);
                Console.WriteLine("\nAuto spędziło na parkingu {0} minut. Oplata wynosi {1} zł.", minuty, parking.podajOplate(minuty));

                parking.usunAuto(wyjezdzajace);   
            }
            catch (ObjectNotFoundException)
            {
                Console.WriteLine("\nNie ma takiego auta");
            }


            czekajNaEnter();
        }

        public bool rejestrujWjazd(string rejestracja)
        {      
            if (string.IsNullOrWhiteSpace(rejestracja))
            {
                return false;
            }
            else
            {
                return parking.dodaj(new Auto(rejestracja));
            }            
        }

        public bool rejestrujWyjazd(string rejestracja, out double min, out double oplata)
        {
            try
            {
              
                double minuty = parking.podajIleCzasuSpedzil(rejestracja).TotalMinutes;
                min = minuty;
                oplata = parking.podajOplate(minuty);
                Auto wyjezdzajace = parking.znajdzAuto(rejestracja);  
                parking.usunAuto(wyjezdzajace);
                return true;
            }
            catch (KeyNotFoundException)
            {
                min = 0;
                oplata = 0;
                return false;
            } 
                       
        }

        void przegladParkingu()
        {
            Console.WriteLine("\nAuta w parkingu");
            Console.WriteLine("\nLiczba aut: " + parking.lbAut());
            // List<Auto> listaAut = parking.GetAuta();

            //for (int i = 0; i < parking.lbAut(); i++)
            //{
            //    Console.WriteLine(parking.GetAuta()[i].nrRej);
            //}

            foreach (var auto in parking.GetAuta())
            {
                Console.WriteLine(auto.nrRej);
            }

            czekajNaEnter();
        }

        public List<AutoModel> PobierzStanParkingu()
        {
            List<AutoModel> listaModeli = new List<AutoModel>();
            var slownik = parking.GetSlownikNrRejCzas();

            foreach (var auto in parking.GetAuta())
            {
                listaModeli.Add(new AutoModel(auto.nrRej, slownik[auto.nrRej].ToString("dd\\/MM\\/yyyy HH:mm:ss"),auto.nadwozie.podajKolor().ToString()));
            }

            return listaModeli;
        }
     
        private void czekajNaEnter()
        {
            Console.WriteLine("\nEnter - kontynuacja");
            Console.ReadKey();
        }       

        private Parking parking;

    }
}
