﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingCSharp
{
    public static class Plik
    {
        public static void ZapiszDoPliku(List<Auto> listaAut, Dictionary<string,DateTime> datyWjazdu)
        {

            string sciezkaDoPliku = Ustawienia.Default.sciezkaDoPliku;
            string dane = string.Empty;

            foreach (var auto in listaAut)
            {
                dane += "Numer:" + auto.nrRej + "\r\n";
                dane += "Kolor:" + auto.nadwozie.podajKolor() + "\r\n";
                dane += "Typ:" + auto.silnik.podajTyp() + "\r\n";
                dane += "Moc:" + auto.silnik.podajMoc() + "\r\n";
                dane += "Data:" + datyWjazdu[auto.nrRej].Ticks + "\r\n";
            }

            File.WriteAllText(sciezkaDoPliku, dane);
        }

        public static void OdczytZPliku(List<Auto> listaAut, Dictionary<string, DateTime> datyWjazdu)
        {
            string sciezkaDoPliku = Ustawienia.Default.sciezkaDoPliku;

            if(!File.Exists(sciezkaDoPliku))
            {
                sciezkaDoPliku = ".\\StanParkingu.txt";
            }

            string[] dane = File.ReadAllLines(sciezkaDoPliku);

            Auto auto = null;

            foreach (var linia in dane)
            {

                if(linia.StartsWith("Numer:"))
                {
                    auto = new Auto(linia.Replace("Numer:", string.Empty));
                }
                else if (linia.StartsWith("Kolor:"))
                {
                    Nadwozie.Kolor kolor;

                    Enum.TryParse(linia.Replace("Kolor:",string.Empty), out kolor);

                    auto.nadwozie.ustawKolor(kolor);                    
                }
                else if (linia.StartsWith("Typ:"))
                {
                    Silnik.Typ typ;

                    Enum.TryParse(linia.Replace("Typ:", string.Empty), out typ);

                    auto.silnik.ustawTyp(typ);
                }
                else if (linia.StartsWith("Moc:"))
                {
                    double moc = Convert.ToDouble(linia.Replace("Moc:", string.Empty));

                    auto.silnik.ustawMoc(moc);
                }
                else if (linia.StartsWith("Data:"))
                {
                    long ticks = Convert.ToInt64(linia.Replace("Data:", string.Empty));

                    DateTime date = new DateTime(ticks);
                    datyWjazdu.Add(auto.nrRej, date);

                    listaAut.Add(auto);         
                }
            }
        }
    }
}
