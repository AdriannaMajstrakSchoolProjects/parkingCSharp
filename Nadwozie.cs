﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingCSharp
{
    public class Nadwozie
    {
        public enum Kolor
        {
            Nieznany,
            Bialy,
            Srebrny,
            // ... inne ..
            Czarny
        }

        public Nadwozie(Kolor k = Kolor.Nieznany)
        {
            kolor = k;
        }

        public Kolor podajKolor()
        {
            return kolor;
        }

        public void ustawKolor(Kolor k)
        {
            kolor = k;
        }

        private Kolor kolor;
    }
}
