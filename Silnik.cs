﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingCSharp
{
    public class Silnik
    {
        public enum Typ
        {
            Nieznany,
            Benzynowy,
            Ropniak,
            Elektryczny,
            Hybryda
        }

        public Silnik(Typ t = Typ.Nieznany, double m = 0)
        {
            typ = t;
            moc = m;
        }

        public Typ podajTyp()
        {
            return typ;
        }

        public double podajMoc()
        {
            return moc;
        }

        public void ustawTyp(Typ t)
        {
            typ = t;
        }

        public void ustawMoc(double m)
        {
            moc = m;
        }

        private Typ typ;
        private double moc;
    }
}
